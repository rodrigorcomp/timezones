import React from 'react';
import { connect } from "react-redux";
import { Link } from 'react-router';
import { signOut } from "../actions/signOutActions";

class Header extends React.Component {

  logout() {
    this.props.dispatch(signOut());
  }

  authHeader () {
    return (
      <form className='navbar-form navbar-right signin-widget'>
        <Link className='btn btn-default' to='/signin'>Sign in</Link>
        <Link className='btn btn-primary' to='/signup'>Sign up</Link>
      </form>
    );
  }

  userArea () {
    return (
      <div className='pull-right'>
        <p className="navbar-text">Signed in as {this.props.user.getIn(['attributes', 'email'])}</p>
        <button
          className='btn btn-danger navbar-btn pull-right'
          onClick={this.logout.bind(this)}>
          Logout
        </button>
      </div>
    );
  }

  adminLinks () {
    if(this.props.user.get('isSignedIn') && this.props.user.getIn(['attributes', 'admin'])){
      return (<li role="presentation">
        <Link to='/users'>Users</Link>
      </li>
      );
    }
  }
  render () {
    return (
      <div className="navbar navbar-default navbar-static-top">
        <div className="container">
          <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <Link to='/' className="navbar-brand">Timezones</Link>
          <ul className="nav navbar-nav">
            {this.adminLinks()}
          </ul>
          <div className="navbar-collapse collapse navbar-responsive-collapse">
            <ul className="nav navbar-nav">
            </ul>
            {this.props.user.get('isSignedIn') ? this.userArea() : this.authHeader()}
          </div>
        </div>
      </div>
    );
  }
}

Header.contextTypes = {
  store: React.PropTypes.object
};

export default connect(({user}) => ({user}))(Header);

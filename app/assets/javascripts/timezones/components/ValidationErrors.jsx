import React from "react";

class ValidationErrors extends React.Component {

  render () {
    var {messages} = this.props;

    if(!messages){
      return null;
    }

    return (
      <div className='alert alert-danger'>
        {messages.valueSeq().map((message, i) => <p key={i}>{message}</p>)}
      </div>
    );
  }
}

export default ValidationErrors;

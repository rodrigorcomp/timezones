import React from 'react';
import { Link, withRouter } from 'react-router';
import ValidationErrors from './ValidationErrors.jsx';
import * as ACTIONS from '../actions/signInActions.js';
import { connect } from "react-redux";


class SignIn extends React.Component {
  handleSubmit (event) {
    event.preventDefault();
    let formData = this.props.signIn.getIn(["form"]).toJS();
    this.props.dispatch(ACTIONS.emailSignIn(formData));
  }

  handleInput (key, event) {
    this.props.dispatch(ACTIONS.emailSignInFormUpdate(key, event.target.value));
  }

  componentWillUpdate(nextProps) {
    if(nextProps.user.get('isSignedIn')){
      this.props.router.push('/');
    }
  }

  render () {
    return (
      <div className='container'>
        <div className='signin-page form-page'>
          <h1>Sign in</h1>

          <ValidationErrors messages={this.props.signIn.getIn(['errors'])} />
          <form onSubmit={this.handleSubmit.bind(this)}>

            <div className='form-group' onSubmit={this.handleSubmit.bind(this)}>
              <label htmlFor='exampleInputEmail1'>Email address</label>
              <input
                id='exampleInputEmail1'
                type='text'
                name='email'
                placeholder='Email'
                className='form-control'
                value={this.props.signIn.getIn(['form', 'email'])}
                onChange={this.handleInput.bind(this, "email")}
                 />
            </div>

            <div className='form-group'>
              <label htmlFor='exampleInputPassword1'>Password</label>
              <input
                id='exampleInputPassword1'
                type='password'
                name='password'
                placeholder='Password'
                value={this.props.signIn.getIn(['form', 'password'])}
                onChange={this.handleInput.bind(this, "password")}
                className='form-control' />
            </div>

            <button type='submit' className='btn btn-success'>Sign in</button>
            <Link className='btn btn-danger' to='/'>Cancel</Link>
          </form>
        </div>
      </div>
    );
  }
}

SignIn.contextTypes = {
  store: React.PropTypes.object
};

export default withRouter(connect(({signIn, user}) => ({signIn, user}))(SignIn));


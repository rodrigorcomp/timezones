Fabricator(:timezone) do
  user
  name { Faker::Name.name }
  city { Faker::Name.name }
  minutes_difference { rand 180 }
end

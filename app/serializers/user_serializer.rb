class UserSerializer < ActiveModel::Serializer
  attributes :id, :name, :uid, :email, :admin
end

import React from 'react';
import Immutable from 'immutable';
import { createReducer } from 'redux-immutablejs';
import * as ACTIONS from '../actions/userFormActions';

const initialState = {
  loading: false,
  errors: null,
  form: {
    name: '',
    password: '',
    password_confirmation: ''
  }
};

export default createReducer(Immutable.fromJS(initialState), {
  [ACTIONS.USER_CREATION_START]: (state) => state.set('loading', true),

  [ACTIONS.USER_CREATION_COMPLETE]: (state) => Immutable.fromJS(initialState),

  [ACTIONS.USER_CREATION_ERROR]: (state, {errors}) => {
    return state.merge({
      loading: false,
      errors
    })
  },

  [ACTIONS.USER_CREATION_FORM_UPDATE]: (state, {key, value}) => {
    return state.mergeDeep({
      form: {
        [key]: value
      }
    });
  },

  [ACTIONS.START_USER_EDITION]: (state, { user }) => {
    return state.merge({
      errors: null,
      form: user
    })
  }
});

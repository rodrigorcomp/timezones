require 'rails_helper'

feature 'Timezones listing', js: true do

  before do
    @user = Fabricate(:user)
    @toronto = Fabricate(:timezone, name: 'Toronto', user: @user)
    @udia = Fabricate(:timezone, name: 'Uberlandia', user: @user)
    Fabricate(:timezone, name: 'NYC', user: Fabricate(:user))

    sign_in_user(@user)
  end

  it 'shows all user\'s timezones ' do
    click_link 'Timezones'

    expect(page).to have_content "Toronto"
    expect(page).to have_content "Uberlandia"
    expect(page).to_not have_content "NYC"
  end

   it 'shows all user\'s timezones ' do
    click_link 'Timezones'

    within ".timezone-tile-#{@toronto.id}" do
      click_button 'Remove'
    end

    expect(page).to_not have_content 'Toronto'
    expect(page).to have_content 'Uberlandia'

    within ".timezone-tile-#{@udia.id}" do
      click_button 'Remove'
    end

    expect(page).to_not have_content 'Toronto'
    expect(page).to_not have_content 'Uberlandia'
  end

end

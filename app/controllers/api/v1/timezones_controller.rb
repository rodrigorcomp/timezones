module API
  module V1
    class TimezonesController < ApiController
      load_and_authorize_resource

      def index
        render json: @timezones.includes(:user)
      end

      def show
        render json: @timezone
      end

      def update
        @timezone.assign_attributes(timezone_params)
        authorize! :update, @timezone

        if @timezone.save
          render json: @timezone
        else
          render json: { errors: @timezone.errors }, status: 422
        end
      end

      def destroy
        @timezone.destroy
        render json: Timezone.all.includes(:user)
      end

      def create
        @timezone.assign_attributes(timezone_params)
        authorize! :create, @timezone

        if @timezone.save
          render json: @timezone
        else
          render json: { errors: @timezone.errors }, status: 422
        end
      end

      private

      def timezone_params
        params.require(:timezone).permit(:name, :city, :minutes_difference, :user_id)
      end
    end
  end
end

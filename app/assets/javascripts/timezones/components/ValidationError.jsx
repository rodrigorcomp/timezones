import React from "react";

class ValidationError extends React.Component {

  render () {
    var {error} = this.props;

    if(!error){
      return null;
    }

    return (
      <span className='form-error'>
        {error}
      </span>
    );
  }
}

export default ValidationError;

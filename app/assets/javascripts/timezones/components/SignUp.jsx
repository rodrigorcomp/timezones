import React from 'react';
import { Link, withRouter } from 'react-router';
import ValidationErrors from './ValidationErrors.jsx';
import * as ACTIONS from '../actions/signUpActions.js';
import { connect } from "react-redux";


class SignUp extends React.Component {

  handleSubmit (event) {
    event.preventDefault();
    let formData = this.props.signUp.getIn(["form"]).toJS();
    this.props.dispatch(ACTIONS.emailSignUp(formData));
  }

  handleInput (key, event) {
    this.props.dispatch(ACTIONS.emailSignUpFormUpdate(key, event.target.value));
  }

  handleCheck (key, event) {
    this.props.dispatch(ACTIONS.emailSignUpFormUpdate(key, event.target.checked));
  }

  componentWillUpdate(nextProps) {
    if(nextProps.user.get('isSignedIn')){
      this.props.router.push('/');
    }
  }

  render () {
    return (
      <div className='container'>

        <div className='signup-page form-page'>
          <h1>Sign up</h1>

          <ValidationErrors messages={this.props.signUp.getIn(['errors', 'full_messages'])} />

          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className='form-group'>
              <label htmlFor='exampleInputEmail1'>Email address</label>
              <input
                type='email'
                name='email'
                className='form-control'
                id='exampleInputEmail1'
                placeholder='Email'
                value={this.props.signUp.getIn(['form', 'email'])}
                onChange={this.handleInput.bind(this, "email")}
                />
            </div>

            <div className='form-group'>
              <label htmlFor='exampleInputPassword1'>Password</label>
              <input
                type='password'
                name='password'
                className='form-control'
                id='exampleInputPassword1'
                placeholder='Password'
                value={this.props.signUp.getIn(['form', 'password'])}
                onChange={this.handleInput.bind(this, "password")}
                />
            </div>

            <div className='form-group'>
              <label htmlFor='exampleInputPassword1'>Password confirmation</label>
              <input
                type='password'
                name='password_confirmation'
                className='form-control'
                id='exampleInputPassword1'
                placeholder='Password'
                value={this.props.signUp.getIn(['form', 'password_confirmation'])}
                onChange={this.handleInput.bind(this, "password_confirmation")}
                />
            </div>

            <div className="checkbox">
              <label>
                <input
                  type='checkbox'
                  name='admin'
                  checked={this.props.signUp.getIn(['form', 'admin'])}
                  onChange={this.handleCheck.bind(this, 'admin')} />
                  Admin?
              </label>
            </div>
            <button type='submit' className='btn btn-default'>Submit</button>
            <Link className='btn btn-danger' to='/'>Cancel</Link>
          </form>
        </div>

      </div>
    );
  }
}

SignUp.contextTypes = {
  store: React.PropTypes.object
};

export default withRouter(connect(({signUp, user}) => ({signUp, user}))(SignUp));

module FeaturesMacros

  def sign_in_user(user = nil, password = '12345678')
    user = Fabricate(:user) unless user

    visit '/'
    click_link 'Sign in'

    within '.signin-page' do
      fill_in 'email', with: user.email
      fill_in 'password', with: password
      click_button 'Sign in'
    end

    user
  end

end

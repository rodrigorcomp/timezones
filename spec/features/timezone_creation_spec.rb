require 'rails_helper'

feature 'Timezone creation', js: true do

  it 'validating timezone form' do
    sign_in_user(Fabricate(:user))

    click_link 'Add Timezone'
    within '.timezone-page' do
      click_button 'Create'
    end

    expect(page).to have_content "can't be blank"
  end

  it 'creates a timezone' do
    sign_in_user(Fabricate(:user))

    click_link 'Add Timezone'
    within '.timezone-page' do
      fill_in 'name', with: 'MyTimezone'
      fill_in 'city', with: 'NYC'
      fill_in 'minutes_difference', with: 180
      click_button 'Create'
    end
    expect(page).to_not have_content "can't be blank"

    click_link 'Timezones'
    expect(page).to have_content 'MyTimezone'
    expect(page).to have_content 'NYC'
  end

end

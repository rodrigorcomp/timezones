require 'rails_helper'

feature 'Timezone edition', js: true do

  before do
    @user = Fabricate(:user)
    Fabricate(:timezone, city: 'Toronto', user: @user)
    sign_in_user @user
  end

  it 'updates a timezone' do
    click_link 'Edit'

    within '.timezone-page' do
      fill_in 'city', with: 'NYC'
      click_button 'Update'
    end
    expect(page).to_not have_content "can't be blank"

    click_link 'Timezones'
    expect(page).to have_content 'NYC'
    expect(page).to_not have_content 'Toronto'
  end

end

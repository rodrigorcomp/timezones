require 'rails_helper'

feature 'Users listing', js: true do
  before do
    @user = Fabricate(:user)
    @admin = Fabricate(:user, admin: true)

    sign_in_user(@admin)

    click_link 'Users'
  end

  it 'editing users' do
    within "#user-#{@user.id}" do
      click_link 'Edit'
    end

    new_email = Faker::Internet.email
    fill_in 'email', with: new_email
    click_button 'Update'

    expect(page).to have_content new_email
    expect(@user.reload.email).to eq new_email
  end
end

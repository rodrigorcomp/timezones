import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import parseResponse from '../utils/parse-response';

export const EMAIL_SIGN_IN_START       = "EMAIL_SIGN_IN_START";
export const EMAIL_SIGN_IN_COMPLETE    = "EMAIL_SIGN_IN_COMPLETE";
export const EMAIL_SIGN_IN_ERROR       = "EMAIL_SIGN_IN_ERROR";
export const EMAIL_SIGN_IN_FORM_UPDATE = "EMAIL_SIGN_IN_FORM_UPDATE";

export function emailSignInFormUpdate(key, value) {
  return { type: EMAIL_SIGN_IN_FORM_UPDATE, key, value };
}
export function emailSignInStart() {
  return { type: EMAIL_SIGN_IN_START };
}
export function emailSignInComplete(user) {
  console.log(user);
  return { type: EMAIL_SIGN_IN_COMPLETE, user };
}
export function emailSignInError(errors) {
  return { type: EMAIL_SIGN_IN_ERROR, errors };
}
export function emailSignIn(body) {
  return dispatch => {
    dispatch(emailSignInStart());

    return fetch(C.SIGN_IN_URL, {
      headers: {
        "Accept": "application/json",
        "Content-Type": "application/json"
      },
      method: "post",
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(({ data }) => dispatch(emailSignInComplete(data)))
      .catch((errors) => dispatch(emailSignInError(errors)));
  };
}

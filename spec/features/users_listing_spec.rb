require 'rails_helper'

feature 'Users listing', js: true do
  before do
    @user = Fabricate(:user)
    @admin = Fabricate(:user, admin: true)

    sign_in_user(@admin)
  end

  it 'shows all users' do
    click_link 'Users'

    expect(page).to have_content @user.email
    expect(page).to have_content @admin.email
  end

  it 'removing user' do
    click_link 'Users'

    expect(page).to have_content @user.email
    expect(page).to have_content @admin.email

    within "#user-#{@user.id}" do
      click_button 'Remove'
    end

    expect(page).to_not have_content @user.email
  end
end

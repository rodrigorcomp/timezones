require 'rails_helper'

feature 'User sign up', js: true do
  it 'validating signup form' do
    visit '/'
    click_link 'Sign up'

    within '.signup-page' do
      fill_in 'email', with: 'rodrigo@email.com'
      fill_in 'password', with: 'password123'
      fill_in 'password_confirmation', with: 'password1234'
      click_button 'Submit'
    end

    expect(page).to have_content "Password confirmation doesn't match"
  end

  it 'signing up and accessing the home page', js: true do
    visit '/'
    click_link 'Sign up'

    within '.signup-page' do
      fill_in 'email', with: 'rodrigo@email.com'
      fill_in 'password', with: 'password123'
      fill_in 'password_confirmation', with: 'password123'
      click_button 'Submit'
    end

    expect(page).to have_content 'Signed in as rodrigo@email.com'
  end

  context 'admin' do
    it 'signing up and accessing the home page', js: true do
      visit '/'
      click_link 'Sign up'

      within '.signup-page' do
        fill_in 'email', with: 'rodrigo@email.com'
        fill_in 'password', with: 'password123'
        fill_in 'password_confirmation', with: 'password123'
        check 'admin'
        click_button 'Submit'
      end

      expect(page).to have_content 'Signed in as rodrigo@email.com'
      expect(page).to have_content 'Users'
    end
  end
end

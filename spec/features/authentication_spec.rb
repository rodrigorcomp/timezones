require 'rails_helper'

feature 'User sign up', js: true do
  it 'validating signup form' do
    visit '/'
    click_link 'Sign in'

    within '.signin-page' do
      fill_in 'email', with: 'rodrigo@email.com'
      fill_in 'password', with: 'password123'
      click_button 'Sign in'
    end

    expect(page).to have_content 'Invalid login credentials. Please try again.'
  end

  it 'signing in and accessing the home page', js: true do
    visit '/'
    click_link 'Sign in'

    Fabricate(:user, email: 'rodrigo@email.com')
    within '.signin-page' do
      fill_in 'email', with: 'rodrigo@email.com'
      fill_in 'password', with: '12345678'
      click_button 'Sign in'
    end

    expect(page).to have_content 'Signed in as rodrigo@email.com'
    expect(page).to_not have_content 'Sign in'
  end
end

feature 'User logout', js: true do
  it 'logging out' do
    user = sign_in_user

    click_button 'Logout'

    expect(page).to_not have_content "Signed in as #{user.email}"
  end
end

require 'cancan/matchers'

describe 'User' do
  describe 'abilities' do
    subject(:ability){ Ability.new(user) }
    let(:user) { Fabricate(:user) }
    let(:other_user) { Fabricate(:user) }

    context 'when is an admin' do
      let(:user) { Fabricate(:user, admin: true) }

      it{ should be_able_to(:read, Fabricate(:timezone, user: user)) }
      it{ should be_able_to(:read, Fabricate(:timezone, user: other_user)) }

      it{ should be_able_to(:manage, User) }
    end

    context 'when is an user' do
      let(:user) { Fabricate(:user, admin: false) }

      it{ should be_able_to(:read, Fabricate(:timezone, user: user)) }
      it{ should_not be_able_to(:read, Fabricate(:timezone, user: other_user)) }

      it{ should_not be_able_to(:read, User) }
    end
  end
end


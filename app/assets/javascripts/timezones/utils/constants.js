import extend from 'extend';

export const SAVED_CREDS_KEY = 'authHeaders';

export const API_PATH = '/api/v1';

export const SIGN_UP_URL =        API_PATH + '/auth/users';
export const SIGN_IN_URL =        API_PATH + '/auth/users/sign_in';
export const SIGN_OUT_URL =       API_PATH + '/auth/users/sign_out';
export const VALIDATE_TOKEN_URL = API_PATH + '/auth/users/validate_token';

export const FETCH_TIMEZONES_URL = API_PATH + '/timezones';
export const CREATE_TIMEZONE_URL = API_PATH + '/timezones';


export const FETCH_USERS_URL = API_PATH + '/users';
export const CREATE_USER_URL = API_PATH + '/users';


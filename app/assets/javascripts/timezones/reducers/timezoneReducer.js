import React from 'react';
import Immutable from 'immutable';
import { createReducer } from 'redux-immutablejs';
import * as ACTIONS from '../actions/timezoneActions';

const initialState = {
  loading: false,
  errors: null,
  form: {
    name: '',
    city: '',
    minutes_difference: ''
  }
};

export default createReducer(Immutable.fromJS(initialState), {
  [ACTIONS.TIMEZONE_CREATION_START]: (state) => state.set('loading', true),

  [ACTIONS.TIMEZONE_CREATION_COMPLETE]: (state) => Immutable.fromJS(initialState),

  [ACTIONS.TIMEZONE_CREATION_ERROR]: (state, {errors}) => {
    return state.merge({
      loading: false,
      errors
    })
  },

  [ACTIONS.TIMEZONE_CREATION_FORM_UPDATE]: (state, {key, value}) => {
    return state.mergeDeep({
      form: {
        [key]: value
      }
    });
  },

  [ACTIONS.START_TIMEZONE_EDITION]: (state, { timezone }) => {
    return state.merge({
      errors: null,
      form: timezone
    })
  }
});

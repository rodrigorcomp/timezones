import React from 'react';
import { Link } from 'react-router';
import * as ACTIONS from '../actions/timezoneActions';
import { connect } from "react-redux";

class Timezone extends React.Component {

  handleRemove() {
    this.props.dispatch(ACTIONS.removeTimezone(this.props.timezone.id));
  }

  ownerHeader () {
    if(this.isAdmin()){
      return <h4 className='owner'> {this.props.timezone.user.email} </h4>
    }
  }

  isAdmin() {
    return this.props.user.getIn(['attributes', 'admin']);
  }

  render () {
    var css = 'col-xs-12 col-sm-6 col-md-4 col-lg-3 timezone-tile timezone-tile-' + this.props.timezone.id;

    return (
      <div className={css}>
        <div className='inner'>
          { this.ownerHeader() }

          <h5>
            {this.props.timezone.name}<br />
            <small>{this.props.timezone.city}</small>
          </h5>

          <div>
            <strong>GMT:</strong> {this.props.timezone.gmt}<br/>
            <strong>Local:</strong> {this.props.timezone.timezone}
          </div>

          <Link className='btn btn-default' to={`timezones/${this.props.timezone.id}/edit`}>Edit</Link>
          <button onClick={this.handleRemove.bind(this)} className='btn btn-default'>Remove</button>
        </div>
      </div>
    );
  }
}

export default connect(({user}) => ({user}))(Timezone);

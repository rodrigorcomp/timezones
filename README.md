# Timezones

Application that shows time in different timezones


## Environment Setup - NPM


This project uses NPM to manage the JS dependencies. Run the following command
to install these dependencies:

```
npm install
```

## Authentication


### Examples of usage

- Signup

```
curl -i -H "Content-Type: application/json" -X POST -d '{ "email": "user@gmail.com", "password": "12345678", "password_confirmation": "12345678" }' http://localhost:3000/api/v1/users
```

- Login

```
curl -i -H "Content-Type: application/json" -X POST -d '{ "email": "user@gmail.com", "password": "12345678" }' http://localhost:3000/api/v1/users/sign_in
```

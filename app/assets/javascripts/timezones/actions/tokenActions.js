import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import parseResponse from '../utils/parse-response';

export const VALIDATE_TOKEN       = 'EMAIL_SIGN_UP_START';
export const UPDATE_TOKEN         = 'EMAIL_SIGN_UP_COMPLETE';

export function updateToken(user) {
  console.log(user);
  return { type: UPDATE_TOKEN, user };
}

export function validateToken() {
  return dispatch => {

    return fetch(C.VALIDATE_TOKEN_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'get'
    })
      .then(parseResponse)
      .then(({data}) => dispatch(updateToken(data)))
      .catch(function() {});
  };
}

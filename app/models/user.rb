class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :validatable

  # This module comes after the `devise` method call, otherwise it'll include
  # undesirable devise modules. Read more:
  # https://github.com/lynndylanhurley/devise_token_auth#excluding-modules
  include DeviseTokenAuth::Concerns::User

  has_many :timezones, dependent: :destroy
end

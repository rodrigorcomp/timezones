//= require jquery
//= require jquery_ujs
//= require twitter/bootstrap
//= require_tree .

import React from 'react';
import ReactDOM from 'react-dom';
import router from './timezones/router.js';

$(function () {
   router();
});


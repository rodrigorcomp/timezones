class Timezone < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :user, :name, :city, :minutes_difference
  validates_numericality_of :minutes_difference, allow_blank: true
end

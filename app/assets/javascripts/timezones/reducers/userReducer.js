import Immutable from "immutable";
import { createReducer } from "redux-immutablejs";
import * as signInActions from "../actions/signInActions";
import * as signOutActions from "../actions/signOutActions";
import * as signUpActions from "../actions/signUpActions";

import * as tokenActions from "../actions/tokenActions";

const initialState = Immutable.fromJS({
  attributes: null,
  isSignedIn: false
});

export default createReducer(initialState, {
  [signUpActions.EMAIL_SIGN_UP_COMPLETE]: (state, { user }) => {
    return Immutable.fromJS({
      attributes: user,
      isSignedIn: true
    });
  },

  [signInActions.EMAIL_SIGN_IN_COMPLETE]: (state, { user }) => {
    console.log(user);
    return Immutable.fromJS({
      attributes: user,
      isSignedIn: true
    });
  },

  [tokenActions.UPDATE_TOKEN]: (state, { user }) => {
    return state.merge({
      attributes: user,
      isSignedIn: true
    });
  },

  [signOutActions.SIGN_OUT_COMPLETE]: state => state.merge(initialState),
  [signOutActions.SIGN_OUT_ERROR]:    state => state.merge(initialState)
});

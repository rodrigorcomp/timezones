import React from 'react';
import { Link, withRouter } from 'react-router';
import ValidationError from './ValidationError.jsx';
import * as ACTIONS from '../actions/timezoneActions.js';
import { connect } from "react-redux";

class TimezoneForm extends React.Component {

  handleSubmit (event) {
    event.preventDefault();
    let formData = this.props.timezone.getIn(["form"]).toJS();

    if(this.props.params.id){
      this.updateTimezone(formData);
    }else{
      this.createTimezone(formData);
    }
  }

  createTimezone(formData) {
    if(this.props.params.user_id){
      formData.user_id = this.props.params.user_id;
    }else{
      formData.user_id = this.props.user.getIn(['attributes', 'id']);
    }

    this.props.dispatch(ACTIONS.createTimezone({ timezone: formData }));
  }

  updateTimezone(formData) {
    this.props.dispatch(ACTIONS.updateTimezone(this.props.params.id, {
      timezone: formData
    }));
  }

  handleInput (key, event) {
    this.props.dispatch(ACTIONS.timezoneCreationFormUpdate(key, event.target.value));
  }

  componentDidMount() {
    if(this.props.params.id){
      this.props.dispatch(ACTIONS.editTimezone(this.props.params.id));
    }else{
      this.props.dispatch(ACTIONS.newTimezone());
    }
  }

  render () {
    if(this.props.params.id){
      var title = 'Update Timezone';
      var action = 'Update';
    }else{
      var title = 'New Timezone';
      var action = 'Create';
    }

    return (
      <div className='container'>
        <div className='timezone-page form-page'>
          <h1>{title}</h1>

          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className='form-group' onSubmit={this.handleSubmit.bind(this)}>
              <label htmlFor='name'>Name</label>
              <input
                id='name'
                type='text'
                name='name'
                placeholder='Name'
                className='form-control'
                value={this.props.timezone.getIn(['form', 'name'])}
                onChange={this.handleInput.bind(this, "name")}
                 />
              <ValidationError error={this.props.timezone.getIn(['errors', 'name'])} />
            </div>

            <div className='form-group'>
              <label htmlFor='city'>City</label>
              <input
                id='city'
                type='text'
                name='city'
                placeholder='City'
                value={this.props.timezone.getIn(['form', 'city'])}
                onChange={this.handleInput.bind(this, "city")}
                className='form-control' />
              <ValidationError error={this.props.timezone.getIn(['errors', 'city'])} />
            </div>

             <div className='form-group'>
              <label htmlFor='minutes_difference'>Difference to GMT (minutes)</label>
              <input
                id='minutes_difference'
                type='text'
                name='minutes_difference'
                placeholder='Difference'
                value={this.props.timezone.getIn(['form', 'minutes_difference'])}
                onChange={this.handleInput.bind(this, "minutes_difference")}
                className='form-control' />
              <ValidationError error={this.props.timezone.getIn(['errors', 'minutes_difference'])} />
            </div>

            <button type='submit' className='btn btn-success'>{action}</button>
            <Link className='btn btn-danger' to='/'>Cancel</Link>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(({timezone, user}) => ({timezone, user}))(TimezoneForm));

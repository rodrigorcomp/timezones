import Cookies from "js-cookie";
import * as C from "./constants";

export function destroySession () {
  Cookies.remove(C.SAVED_CREDS_KEY, { path:  "/" });
}

export function persistData (key, val) {
  val = JSON.stringify(val);

  Cookies.set(key, val, {
    expires: 14,
    cookiePath: '/'
  });
};

export function retrieveData (key) {
  var val = Cookies.get(key);

  // if value is a simple string, the parser will fail. in that case, simply
  // unescape the quotes and return the string.
  try {
    // return parsed json response
    return JSON.parse(val);
  } catch (err) {
    // unescape quotes
    return null;
  }
};

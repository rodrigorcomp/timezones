class Ability
  include CanCan::Ability

  def initialize(user)
    if user.admin?
      can :manage, User
      can :manage, Timezone
    else
      can :manage, Timezone, user_id: user.id
    end
  end
end

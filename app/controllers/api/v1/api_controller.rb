module API
  module V1
    class ApiController < ApplicationController
      include DeviseTokenAuth::Concerns::SetUserByToken
      before_action :authenticate_user!
      check_authorization

      rescue_from CanCan::AccessDenied do |exception|
        render json: { errors: [ 'unauthorized' ] }, status: 403
      end
    end
  end
end

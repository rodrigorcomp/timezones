import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import parseResponse from '../utils/parse-response';

export const EMAIL_SIGN_UP_START       = 'EMAIL_SIGN_UP_START';
export const EMAIL_SIGN_UP_COMPLETE    = 'EMAIL_SIGN_UP_COMPLETE';
export const EMAIL_SIGN_UP_ERROR       = 'EMAIL_SIGN_UP_ERROR';
export const EMAIL_SIGN_UP_FORM_UPDATE = 'EMAIL_SIGN_UP_FORM_UPDATE';

export function emailSignUpFormUpdate(key, value) {
  return { type: EMAIL_SIGN_UP_FORM_UPDATE, key, value };
}

export function emailSignUpStart() {
  return { type: EMAIL_SIGN_UP_START };
}

export function emailSignUpComplete(user) {
  return { type: EMAIL_SIGN_UP_COMPLETE, user };
}

export function emailSignUpError(errors) {
  return { type: EMAIL_SIGN_UP_ERROR, errors };
}

export function emailSignUp(body) {
  return dispatch => {
    dispatch(emailSignUpStart());

    return fetch(C.SIGN_UP_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(({data}) => dispatch(emailSignUpComplete(data)))
      .catch(({errors}) => dispatch(emailSignUpError(errors)));
  };
}

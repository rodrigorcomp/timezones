import originalFetch from "isomorphic-fetch";
import * as C from "./constants";
import extend from "extend";
import { retrieveData, persistData } from "./session-storage";

function getAuthHeaders() {
  // fetch current auth headers from storage
  var currentHeaders = retrieveData(C.SAVED_CREDS_KEY) || {},
      nextHeaders = {};

  // bust IE cache
  nextHeaders["If-Modified-Since"] = "Mon, 26 Jul 1997 05:00:00 GMT";

  // set `access-token` header
  nextHeaders['access-token'] = currentHeaders['access-token'];
  nextHeaders['uid'] = currentHeaders['uid'];
  nextHeaders['client'] = currentHeaders['client'];

  console.log("nextHeaders: ", nextHeaders);
  return nextHeaders;
}

function updateAuthCredentials(resp) {
  var accessToken = resp.headers.get('access-token');

  if(accessToken) {
    var newHeaders = {
      'access-token': resp.headers.get('access-token'),
      'uid': resp.headers.get('uid'),
      'client': resp.headers.get('client'),
    };

    console.log("newHeaders:", newHeaders);

    // persist headers for next request
    persistData(C.SAVED_CREDS_KEY, newHeaders);
  }

  return resp;
}

export default function (url, options={}) {
  if (!options.headers) {
    options.headers = {}
  }

  extend(options.headers, getAuthHeaders());

  return originalFetch(url, options)
    .then(resp => updateAuthCredentials(resp));
}

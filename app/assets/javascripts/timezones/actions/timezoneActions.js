import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import * as timezonesActions from "./timezonesActions";
import parseResponse from '../utils/parse-response';
import { push } from 'react-router-redux';


export const TIMEZONE_CREATION_START        = 'TIMEZONE_CREATION_START';
export const TIMEZONE_CREATION_COMPLETE     = 'TIMEZONE_CREATION_COMPLETE';
export const TIMEZONE_CREATION_ERROR        = 'TIMEZONE_CREATION_ERROR';
export const TIMEZONE_CREATION_FORM_UPDATE  = 'TIMEZONE_CREATION_FORM_UPDATE';

export const START_TIMEZONE_EDITION         = 'START_TIMEZONE_EDITION';

export function timezoneCreationFormUpdate(key, value) {
  return { type: TIMEZONE_CREATION_FORM_UPDATE, key, value };
}

export function timezoneCreationStart() {
  return { type: TIMEZONE_CREATION_START };
}

export function timezoneCreationComplete() {
  console.log('timezoneCreationComplete');
  return { type: TIMEZONE_CREATION_COMPLETE };
}

export function newTimezone() {
  return { type: TIMEZONE_CREATION_COMPLETE };
}

export function timezoneCreationError(errors) {
  return { type: TIMEZONE_CREATION_ERROR, errors };
}

export function startTimezoneEdition(timezone) {
  return { type: START_TIMEZONE_EDITION, timezone };
}

export function editTimezone(id) {
  return dispatch => {
    return fetch(C.FETCH_TIMEZONES_URL + '/' + id , {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'get',
    })
      .then(parseResponse)
      .then(({timezone}) => dispatch(startTimezoneEdition(timezone)))
      .catch(function() {});
  }
}

export function createTimezone(body) {
  return dispatch => {
    dispatch(timezoneCreationStart());

    return fetch(C.CREATE_TIMEZONE_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(() => {
        dispatch(push('/'))
        dispatch(timezoneCreationComplete())
      })
      .catch(({errors}) => dispatch(timezoneCreationError(errors)));
  };
}

export function updateTimezone(id, body) {
  return dispatch => {
    dispatch(timezoneCreationStart());

    return fetch(C.FETCH_TIMEZONES_URL + '/' + id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'put',
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(() => {
        dispatch(timezoneCreationComplete());
        dispatch(push('/'))
      })
      .catch(({errors}) => dispatch(timezoneCreationError(errors)));
  };
}

export function removeTimezone(id) {
  return dispatch => {
    return fetch(C.FETCH_TIMEZONES_URL + '/' + id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'delete'
    })
      .then(parseResponse)
      .then(({timezones}) => dispatch(timezonesActions.fetchTimezonesComplete(timezones)))
      .catch(function() {});
  };
}


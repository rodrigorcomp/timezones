module API
  module V1
    class UsersController < ApiController
      load_and_authorize_resource

      def index
        render json: @users
      end

      def show
        render json: @user
      end

      def update
        if @user.update(user_params)
          render json: @user
        else
          render json: { errors: @user.errors }, status: 422
        end
      end

      def destroy
        @user.destroy
        render json: User.all
      end

      def create
        @user.assign_attributes(user_params)

        if @user.save
          render json: @user
        else
          render json: { errors: @user.errors }, status: 422
        end
      end

      private

      def user_params
        params.require(:user).permit(:email, :password, :password_confirmation)
      end
    end
  end
end

import React from 'react';
import { Link } from 'react-router';
import * as ACTIONS from '../actions/timezonesActions';
import { connect } from "react-redux";
import Timezone from "./Timezone.jsx";

class Timezones extends React.Component {

  componentDidMount(nextProps) {
    if(this.props.user.get('isSignedIn')){
      this.props.dispatch(ACTIONS.fetchTimezones());
    }
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.user.get('isSignedIn') && nextProps.user.get('isSignedIn')){
      this.props.dispatch(ACTIONS.fetchTimezones());
    }
  }

  actions() {
    if(this.props.user.get('isSignedIn')) {
      return <Link className='btn btn-default' to='/timezones/new'>Add Timezone</Link>;
    }
  }

  render () {
    var timezones = this.props.timezones.get('list').map((timezone, i) => <Timezone key={i} timezone={timezone} />);

    return (
      <div className='container'>
        <div className='list-actions row'>
          <div className='col-md-12'>
            { this.actions() }
          </div>
        </div>

        <div className='row'>
          {timezones}
        </div>
      </div>
    );
  }
}

export default connect(({timezones, user}) => ({timezones, user}))(Timezones);

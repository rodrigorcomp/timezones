import React from 'react';
import { Link } from 'react-router';
import * as ACTIONS from '../actions/usersActions';
import * as USER_ACTIONS from '../actions/userFormActions';
import { connect } from "react-redux";

class Users extends React.Component {

  componentDidMount(nextProps) {
    if(this.props.user.get('isSignedIn')){
      this.props.dispatch(ACTIONS.fetchUsers());
    }else{

    }
  }

  componentWillReceiveProps(nextProps) {
    if(!this.props.user.get('isSignedIn') && nextProps.user.get('isSignedIn')){
      this.props.dispatch(ACTIONS.fetchUsers());
    }
  }

  removeUser (userId) {
    this.props.dispatch(USER_ACTIONS.removeUser(userId));
  }

  render () {
    var users = this.props.users.get('list').map((user, i) =>
      <li className='list-group-item' id={'user-' + user.id} key={i}>
        {user.email}
        <button className='btn btn-danger' onClick={this.removeUser.bind(this, user.id)}>Remove</button>
        <Link className='btn btn-default' to={`users/${user.id}/edit`}>Edit</Link>
        <Link className='btn btn-default' to={`users/${user.id}/timezones/new`}>Add Timezone</Link>
      </li>
    );

    return (
      <div className='container'>
        <div className='list-actions row'>
          <div className='col-md-12'>
            <Link className='btn btn-default' to='/users/new'>Add User</Link>
          </div>
        </div>

        <div className='row'>
          <div className='col-md-12'>
            <h2>Users</h2>
            <ul className='list-group'> {users} </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(({users, user}) => ({users, user}))(Users);

import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import parseResponse from '../utils/parse-response';

export const FETCH_USERS_START       = 'FETCH_USERS_START';
export const FETCH_USERS_COMPLETE    = 'FETCH_USERS_COMPLETE';

export function fetchUsersStart() {
  return { type: FETCH_USERS_START };
}

export function fetchUsersComplete(users) {
  console.log(users);
  return { type: FETCH_USERS_COMPLETE, users };
}

export function fetchUsers() {
  return dispatch => {
    dispatch(fetchUsersStart());

    return fetch(C.FETCH_USERS_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'get'
    })
      .then(parseResponse)
      .then(({users}) => dispatch(fetchUsersComplete(users)))
      .catch(function() {});
  };
}

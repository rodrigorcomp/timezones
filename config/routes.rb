Rails.application.routes.draw do
  root to: 'home#index'

  mount_devise_token_auth_for 'User', at: 'api/v1/auth/users'

  namespace :api do
    namespace :v1 do
      resources :timezones
      resources :users, except: [:edit]
    end
  end
end

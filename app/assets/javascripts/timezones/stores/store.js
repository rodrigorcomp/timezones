import { createStore, combineReducers, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import ReduxThunk from 'redux-thunk';
import signUpReducer from "../reducers/signUpReducer";
import signInReducer from "../reducers/signInReducer";
import userReducer from "../reducers/userReducer";
import timezonesReducer from "../reducers/timezonesReducer";
import timezoneReducer from "../reducers/timezoneReducer";
import usersReducer from "../reducers/usersReducer";
import userFormReducer from "../reducers/userFormReducer";

export default function configureStore(hashHistory, initialState) {
  const combinedReducers = combineReducers({
    signUp: signUpReducer,
    signIn: signInReducer,
    user: userReducer,
    timezones: timezonesReducer,
    timezone: timezoneReducer,
    users: usersReducer,
    userForm: userFormReducer
  });

  const historyMiddleware = routerMiddleware(hashHistory);

  return createStore(
    combinedReducers,
    initialState,
    applyMiddleware(historyMiddleware, ReduxThunk));
}

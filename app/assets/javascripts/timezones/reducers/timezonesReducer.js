import React from 'react';
import Immutable from 'immutable';
import { createReducer } from 'redux-immutablejs';
import * as ACTIONS from '../actions/timezonesActions';
import * as signOutActions from "../actions/signOutActions";

const initialState = {
  list: []
};

export default createReducer(Immutable.fromJS(initialState), {
  [ACTIONS.FETCH_TIMEZONES]: (state) =>  state.merge(initialState),

  [ACTIONS.FETCH_TIMEZONES_COMPLETE]: (state, {timezones}) => {
    return state.set('list', timezones);
  },

  [signOutActions.SIGN_OUT_COMPLETE]: state => state.merge(initialState),
});

import React from 'react';
import Header from './Header.jsx';
import { validateToken } from '../actions/tokenActions.js';
import { connect } from "react-redux";

class TimezonesApp extends React.Component {
  componentWillMount() {
    this.props.dispatch(validateToken());
  }

  render () {
    return (
      <div>
        <Header />

        {this.props.children}
      </div>
    );
  }
}

TimezonesApp.contextTypes = {
  store: React.PropTypes.object
};

// export default TimezonesApp;
export default connect()(TimezonesApp);

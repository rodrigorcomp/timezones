class TimezoneSerializer < ActiveModel::Serializer
  attributes :name, :city, :timezone, :gmt, :id, :minutes_difference

  has_one :user

  def gmt
    localize Time.current
  end

  def timezone
    localize Time.current + object.minutes_difference.minutes
  end

  private

  def localize(time)
    I18n.l time.in_time_zone('GMT'), format: :long
  end
end

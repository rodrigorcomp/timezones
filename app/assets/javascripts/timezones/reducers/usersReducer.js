import React from 'react';
import Immutable from 'immutable';
import { createReducer } from 'redux-immutablejs';
import * as ACTIONS from '../actions/usersActions';
import * as signOutActions from "../actions/signOutActions";

const initialState = {
  list: []
};

export default createReducer(Immutable.fromJS(initialState), {
  [ACTIONS.FETCH_USERS_START]: (state) =>  state.merge(initialState),

  [ACTIONS.FETCH_USERS_COMPLETE]: (state, {users}) => {
    return state.set('list', users);
  },

  [signOutActions.SIGN_OUT_COMPLETE]: state => state.merge(initialState),
});

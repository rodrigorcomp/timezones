import React from 'react';
import { Link, withRouter } from 'react-router';
import ValidationError from './ValidationError.jsx';
import * as ACTIONS from '../actions/userFormActions.js';
import { connect } from "react-redux";

class UserForm extends React.Component {

  handleSubmit (event) {
    event.preventDefault();
    let formData = this.props.userForm.getIn(["form"]).toJS();

    if(this.props.params.id){
      this.updateUser(formData);
    }else{
      this.createUser(formData);
    }
  }

  createUser(formData) {
    this.props.dispatch(ACTIONS.createUser({ user: formData }));
  }

  updateUser(formData) {
    this.props.dispatch(ACTIONS.updateUser(this.props.params.id, {
      user: formData
    }));
  }

  handleInput (key, event) {
    this.props.dispatch(ACTIONS.userCreationFormUpdate(key, event.target.value));
  }

  componentDidMount() {
    if(this.props.params.id){
      this.props.dispatch(ACTIONS.editUser(this.props.params.id));
    }else{
      this.props.dispatch(ACTIONS.newUser());
    }
  }

  render () {
    if(this.props.params.id){
      var title = 'Update User';
      var action = 'Update';
    }else{
      var title = 'New User';
      var action = 'Create';
    }

    return (
      <div className='container'>
        <div className='timezone-page form-page'>
          <h1>{title}</h1>

          <form onSubmit={this.handleSubmit.bind(this)}>
            <div className='form-group' onSubmit={this.handleSubmit.bind(this)}>
              <label htmlFor='email'>Email</label>
              <input
                id='email'
                type='text'
                name='email'
                placeholder='Email'
                className='form-control'
                value={this.props.userForm.getIn(['form', 'email'])}
                onChange={this.handleInput.bind(this, "email")}
                 />
              <ValidationError error={this.props.userForm.getIn(['errors', 'email'])} />
            </div>

            <div className='form-group'>
              <label htmlFor='password'>Password</label>
              <input
                id='password'
                type='text'
                name='password'
                placeholder='Password'
                value={this.props.userForm.getIn(['form', 'password'])}
                onChange={this.handleInput.bind(this, "password")}
                className='form-control' />
              <ValidationError error={this.props.userForm.getIn(['errors', 'password'])} />
            </div>

             <div className='form-group'>
              <label htmlFor='password_confirmation'>Password confirmation</label>
              <input
                id='password_confirmation'
                type='text'
                name='password_confirmation'
                placeholder='Password confirmation'
                value={this.props.userForm.getIn(['form', 'password_confirmation'])}
                onChange={this.handleInput.bind(this, "password_confirmation")}
                className='form-control' />
              <ValidationError error={this.props.userForm.getIn(['errors', 'password_confirmation'])} />
            </div>

            <button type='submit' className='btn btn-success'>{action}</button>
            <Link className='btn btn-danger' to='/'>Cancel</Link>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(connect(({userForm, user}) => ({userForm, user}))(UserForm));

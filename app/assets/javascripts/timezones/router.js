"use strict";

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';

import TimezonesApp from './components/TimezonesApp.jsx';
import Timezones from './components/Timezones.jsx';
import SignUp from './components/SignUp.jsx';
import SignIn from './components/SignIn.jsx';
import TimezoneForm from './components/TimezoneForm.jsx';
import Users from './components/Users.jsx';
import UserForm from './components/UserForm.jsx';

import configureStore from './stores/store';

var runRouter = function(){

  var store = configureStore(hashHistory);

  ReactDOM.render(
    <Provider store={store}>
      <Router history={hashHistory}>
        <Route path="/" component={TimezonesApp} >
          <IndexRoute component={Timezones} />
          <Route path="/signup" component={SignUp} />
          <Route path="/signin" component={SignIn} />
          <Route path="/timezones/new" component={TimezoneForm} />
          <Route path="/timezones/:id/edit" component={TimezoneForm} />
          <Route path="/users" component={Users} />
          <Route path="/users/new" component={UserForm} />
          <Route path="/users/:id/edit" component={UserForm} />
          <Route path="/users/:user_id/timezones/new" component={TimezoneForm} />
        </Route>
      </Router>
    </Provider>
  , document.getElementById('app-root'));
}



export default runRouter;

import React from 'react';
import Immutable from 'immutable';
import { createReducer } from 'redux-immutablejs';
import extend from 'extend';
import * as ACTIONS from '../actions/signUpActions';

const initialState = {
  logged: false,
  loading: false,
  errors: null,
  form: {
    email: '',
    password: '',
    password_confirmation: ''
  }
};

export default createReducer(Immutable.fromJS(initialState), {
  [ACTIONS.EMAIL_SIGN_UP_START]: (state) => state.setIn(['loading'], true),

  [ACTIONS.EMAIL_SIGN_UP_COMPLETE]: (state, {form}) => {
    return state.merge({
      loading: false,
      errors: null,
      logged: true,
      form
    });
  },

  [ACTIONS.EMAIL_SIGN_UP_ERROR]: (state, {errors}) => state.merge({
    loading: false,
    errors
  }),

  [ACTIONS.EMAIL_SIGN_UP_FORM_UPDATE]: (state, {key, value}) => {
    return state.mergeDeep({
      form: {
        [key]: value
      }
    });
  }
});

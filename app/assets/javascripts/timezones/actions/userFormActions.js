import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import * as usersActions from "./usersActions";
import parseResponse from '../utils/parse-response';
import { push } from 'react-router-redux';


export const USER_CREATION_START        = 'USER_CREATION_START';
export const USER_CREATION_COMPLETE     = 'USER_CREATION_COMPLETE';
export const USER_CREATION_ERROR        = 'USER_CREATION_ERROR';
export const USER_CREATION_FORM_UPDATE  = 'USER_CREATION_FORM_UPDATE';

export const START_USER_EDITION         = 'START_TIMEZONE_EDITION';

export function userCreationFormUpdate(key, value) {
  return { type: USER_CREATION_FORM_UPDATE, key, value };
}

export function userCreationStart() {
  return { type: USER_CREATION_START };
}

export function userCreationComplete() {
  return { type: USER_CREATION_COMPLETE };
}

export function newUser() {
  return { type: USER_CREATION_COMPLETE };
}

export function userCreationError(errors) {
  return { type: USER_CREATION_ERROR, errors };
}

export function startUserEdition(user) {
  console.log('startUserEdition', user);
  return { type: START_USER_EDITION, user };
}

export function editUser(id) {
  return dispatch => {
    return fetch(C.FETCH_USERS_URL + '/' + id , {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'get',
    })
      .then(parseResponse)
      .then(({user}) => dispatch(startUserEdition(user)))
      .catch(function(e) { console.error(e) });
  }
}

export function createUser(body) {
  return dispatch => {
    dispatch(userCreationStart());

    return fetch(C.CREATE_USER_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'post',
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(() => {
        dispatch(push('/users'))
        dispatch(userCreationComplete())
      })
      .catch(({errors}) => dispatch(userCreationError(errors)));
  };
}

export function updateUser(id, body) {
  return dispatch => {
    dispatch(userCreationStart());

    return fetch(C.FETCH_USERS_URL + '/' + id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'put',
      body: JSON.stringify(body)
    })
      .then(parseResponse)
      .then(() => {
        dispatch(userCreationComplete());
        dispatch(push('/users'))
      })
      .catch(({errors}) => dispatch(userCreationError(errors)));
  };
}

export function removeUser(id) {
  return dispatch => {
    return fetch(C.FETCH_USERS_URL + '/' + id, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'delete'
    })
      .then(parseResponse)
      .then(({users}) => dispatch(usersActions.fetchUsersComplete(users)))
      .catch(function(e) { console.error(e) });
  };
}


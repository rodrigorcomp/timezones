import fetch from '../utils/fetch';
import * as C from "../utils/constants";
import parseResponse from '../utils/parse-response';

export const FETCH_TIMEZONES             = 'FETCH_TIMEZONES';
export const FETCH_TIMEZONES_START       = 'FETCH_TIMEZONES_START';
export const FETCH_TIMEZONES_COMPLETE    = 'FETCH_TIMEZONES_COMPLETE';

export function fetchTimezonesStart() {
  return { type: FETCH_TIMEZONES_START };
}

export function fetchTimezonesComplete(timezones) {
  return { type: FETCH_TIMEZONES_COMPLETE, timezones };
}

export function fetchTimezones() {
  return dispatch => {
    dispatch(fetchTimezonesStart());

    return fetch(C.FETCH_TIMEZONES_URL, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'get'
    })
      .then(parseResponse)
      .then(({timezones}) => dispatch(fetchTimezonesComplete(timezones)))
      .catch(function() {});
  };
}
